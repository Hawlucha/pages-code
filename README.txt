# hawlucha.codeberg.page
Art blog written with Jekyll.
Work in progress.

This repository contains the site code compiled from Jekyll. [The Jekyll code is here](https://codeberg.org/Hawlucha/my-site-code).


## License

The content of this project itself is licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/), and the underlying source code used to format and display that content is licensed under the [MIT license](LICENSE). Specific content may be given explicit licenses depending on their use case.
